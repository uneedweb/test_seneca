import React, { Component } from 'react';
import './App.css';
import ToggleWrapper from './components/Toggler/ToggleWrapper';
import { createStore } from 'redux';
import { switcherReducer } from './reducers/toggler';
import { Provider } from 'react-redux';
import { combineReducers } from 'redux'

const reducers = combineReducers({
  switcher: switcherReducer,
})

const store = createStore(reducers);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="app">
          <ToggleWrapper />
        </div> 
      </Provider>
    );
  }
}

export default App;
