const defaultState = [
    {
        id: 1,
        left: 'good',
        right: 'bad',
        isChecked: true,
        toBeEqualTo: true,
    },
    {
        id: 2,
        left: 'lot',
        right: 'less',
        isChecked: true,
        toBeEqualTo: true,
    },
    {
        id: 3,
        left: 'expensive',
        right: 'free',
        isChecked: true,
        toBeEqualTo: false,
    },
    {
        id: 4,
        left: 'left',
        right: 'right',
        isChecked: true,
        toBeEqualTo: false,
    }
]

export const switcherReducer = (state = defaultState, action) => {
    switch(action.type) {
        case 'TOGGLE_ITEM':
            let item = state.find(({ id }) => id === action.payload.id);
            let itemIndex = state.indexOf(item);
            let newItem = {
                ...item,
                isChecked: !item.isChecked,
            };

            return [
                ...state.slice(0, itemIndex),
                newItem,
                ...state.slice(itemIndex + 1),
            ];
        default:
            return defaultState;
    }
}