import React, { Component } from 'react';
import ToggleItem from './ToggleItem';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { toggleItem } from '../actions/switcher';
import { validateSwitcher } from '../../utils/validation';

class ToggleWrapper extends Component {

    isValid = () => validateSwitcher(this.props.switcher);

    render() {
        const { switcher, toggleItem } = this.props;
        const isInvalid = !this.isValid();
        const validationMessage = isInvalid ? 'The answer is incorrect.' : 'The answer is correct.';
        const wrapperClassName = classNames('toggle_wrapper', {
            'toggle_wrapper--incorrect': isInvalid,
        });
        const renderedItems = switcher.map(item => 
            <ToggleItem 
                key={item.id}
                item={item}
                onToggleItem={toggleItem}
            />
        );

        return (
            <div className={wrapperClassName}>
                {renderedItems}
                <div className="toggle_wrapper__validation">{validationMessage}</div>
            </div>
        )
    }
}

const mapStateToProps = ({ switcher }) => ({
    switcher,
});

export default connect(mapStateToProps, { toggleItem })(ToggleWrapper);