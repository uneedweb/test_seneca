import React, { Component } from 'react';
import classNames from 'classnames';

class ToggleItem extends Component {

    toggleItem = () => {
        const { item: { id }, onToggleItem } = this.props;
        onToggleItem(id);
    }

    render() {
        const { item: { left, right, isChecked } } = this.props;
        const className = classNames('toggle_wrapper__item', {
            'active': isChecked,
        });

        return (      
            <div className={className} onClick={this.toggleItem}>
                <div className="toggle_value left">{left}</div>
                <div className="toggle_value right">{right}</div>
            </div>
        )
    }
}

export default ToggleItem;