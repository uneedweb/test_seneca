export const toggleItem = id => {
    return {
        type: 'TOGGLE_ITEM',
        payload: {
            id,
        }
    }
}