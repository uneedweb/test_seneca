import isEmpty from 'validator/lib/isEmpty';

export const validateSwitcher = (state) => {
    const isValid = state.every(({ toBeEqualTo, isChecked }) => toBeEqualTo === isChecked);
    
    return isValid;
}